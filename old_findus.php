<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="">
    <title>Care n Cure Pharmacy</title>
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="stylesheet/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/fancybox.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/style.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/colors/color1.css" id="colors"> 
    <link rel="stylesheet" type="text/css" href="stylesheet/responsive.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/animate.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/et-line.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/jquery-ui.css">
    <script type="text/javascript" src="js/jquery-3.3.1.js"></script>
</head>
<body>
    <div class="boxed">
        <div class="top style2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-page">
                            <a href="#" target="_self" class="tp-caption flat-button-slider bg-blue"data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'data-x="['left','left','left','left']" data-hoffset="['35','35','35','15']"data-y="['middle','middle','middle','middle']" data-voffset="['130','140','140','110']"data-fontsize="['14','14','14','14']"data-width="['auto']"data-height="['auto']">Division of Care n Cure Group </a>
                        </div>
                        <ul class="flat-social">
                            <li> <a href="#" title=""><i class="fa fa-facebook-f"></i></a> </li>
                            <li> <a href="#" title=""><i class="fa fa-google-plus"></i></a> </li>
                            <li> <a href="#" title=""><i class="fa fa-linkedin"></i></a> </li>
                            <li> <a href="#" title=""><i class="fa fa-rss"></i></a> </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div id="logo" class="logo">
                            <a href="#" title="">
                                <img src="images/logo.png" alt="" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="iconbox style2 v2">
                            <div class="iconbox-icon">
                                <i class="fa fa-paper-plane-o"></i>
                            </div>
                            <div class="iconbox-content">
                                <h4>+ 974 44507442/3</h4>
                                <p>info@carencureqatar.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="iconbox style2 v2">
                            <div class="iconbox-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <div class="iconbox-content">
                                <h4>+ 974 44507442/3  </h4>
                                <p> Customer Care</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <header id="header" class="header style1 v1 bg-color">
            <div class="container">
                <div class="row">
                    <div class="header-wrap">
                        <div class="col-md-12">
                            <div class="flat-header-wrap">
                                <div class="nav-wrap">
                                    <div class="btn-menu">
                                        <span></span>
                                    </div>
                                    <nav id="mainnav" class="mainnav">
                                        <ul class="menu">
                                            <li> <a href="http://www.pharmacy.technoastra.com/" title="">Home</a> </li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/find-us/" title="">FIND US</a> </li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/med-club/" title="">MED CLUB</a> </li>
                                            <li> <a href="http://carencurewebstore.com/" target="_blank" title="">WEBSTORE</a> </li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/news-events/"  title="">News & Events</a> </li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/promotion/"  title=""> promotion</a></li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/contact-us/" title="">CONTACT US</a> </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="page-title parallax parallax2">
            <div class="title-heading">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title-heading">
                                <h1 class="h1-title">FIND US</h1>
                            </div>
                            <ul class="breadcrumbs">
                                <li><a href="#" title="">Home<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                <li>Find us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="flat-row flat-contact" style="padding-top:15px; padding-bottom:30px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="border-bottom: solid 1px #136bcc; background:#f9f6f6; padding-top:20px;">
                        <div class="flat-title style1 center">
                            <h2>FIND THE NEAREST PHARMACY </h2>
                        </div>
                        <div class="flat-contact-form" style="padding:0px; margin-top:0px;">
                            <form id=""  method="post" action="" class="form-info">
                                <div class="field-row">
                                    <div class="one-three">
                                        <p class="input-info">
                                            <select data-trigger="" name="Location" id="location_id">
                                                <option value="">Location</option>
                                            </select>
                                        </p>

                                    </div>
                                    <div class="one-three">
                                        <p class="input-info">
                                            <select data-trigger="" name="Showroom" id="showroom_id">
                                                <option value="">Showroom</option>
                                            </select>
                                        </p>
                                    </div>
                                    <div class="btn-submit">
                                        <button type="button" id="find_button">FIND US</button>
                                    </div>
                                </div>
                            </form> 
                        </div>
                    </div> 
                </div>
            </div>
        </section>
        <section class="main-services" style="padding-top:0px;">
            <div class="container">
                <div class="row">
                    <div class="wrap-services-1">
                        <div class="services-content-tab" style="width:100%;">
                            <div class="content-inner">
                                <div class="one-half">
                                    <div class="text-services">
                                        <h3 id="location_name">Abu Hamour </h3>
                                        <div class="widget-map">
                                            <h3></h3>
                                            <ul class="">
                                                <li class="address"> <span id="showroom_name">CARE N CURE ABU HAMOUR PHARMACY</span><br> <span id="showroom_landmark">At Abu Hamour Petrol station</span> </li>
                                                <li class="phone" id="showroom_contact"> +974 44 124 711 </li>
                                                <li class="clock"><img style=" width: 53px; height: 53px; " src="http://www.pharmacy.technoastra.com/wp-content/themes/carencure/images/24icon.png" id="working_hours" alt=""> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="one-half">
                                    <div class="owl-carousel-4" id="image_area">
                                        <div class="images-item">
                                            <img src="http://www.pharmacy.technoastra.com/wp-content/themes/carencure/images/page/services-01.jpg" alt="">
                                        </div>
                                        <div class="images-item">
                                            <img src="http://www.pharmacy.technoastra.com/wp-content/themes/carencure/images/page/services-01.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="related-post">
                                    <div class="flat-title">
                                        <iframe id="showroom_address" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1804.4824524574628!2d51.4993385!3d25.238107!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e45d016a4d24151%3A0x1162d08b3c755e24!2sCare+n+Cure+Pharmacy!5e0!3m2!1sen!2sqa!4v1540381983033" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-ft widget-about">
                            <div id="logo-ft">
                                <a href="#" title="">
                                    <img src="images/logo-ft.png" alt="">
                                </a>
                            </div>
                            <p>We have a dedicated corporate division that caters to the medical needs of over 96 major corporate clients in Qatar. We provide our corporate clients with customized health . 
                            </p>
                            <ul class="social">
                                <li> <a href="#" title=""> <i class="fa fa-facebook-f"></i> </a> </li>
                                <li> <a href="#" title=""> <i class="fa fa-twitter"></i> </a> </li>
                                <li> <a href="#" title=""> <i class="fa fa-linkedin"></i> </a> </li>
                                <li> <a href="#" title=""> <i class="fa fa-google-plus"></i> </a> </li>
                            </ul>   
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-ft widget-services">
                            <h3 class="title">Quicklink</h3>
                            <ul class="one-half first">
                                <li><a href="#" title="">Home</a></li>
                                <li><a href="#" title="">Promotion</a></li>
                                <li> <a href="" title=""> Med Club</a> </li>
                            </ul>
                            <ul class="one-half">
                                <li><a href="#" title="">Find us</a></li>
                                <li><a href="#" title="">Contact us</a></li>
                                <li><a href="#" title="">News & Evants</a></li>
                                <li> <a href="http://carencurewebstore.com/" target="_blank" title="">Webstore</a> </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-ft widget-lastest">
                            <h3 class="title">Latest Twitter</h3>
                            <a class="twitter-timeline" data-height="200" data-theme="dark" href="https://twitter.com/CareNCureQatar">Tweets by bloom_entertain</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-ft widget-subscribe">
                            <h3 class="title"> Get in touch with us</h3>
                            CARE N CURE QATAR <br>
                            P. O. Box 23094, Doha-Qatar<br>
                            T: + 974 44507442/3 <br>
                            F: +974 44507445<br>
                            E: info@carencureqatar.com <br>
                            W: www.carencureqatar.com<br>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>© Copyright <a href="#" title="">Care n Cure 2018</a>. All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-go-top">
            <a href="#" title="" class="go-top"> <i class="fa fa-chevron-up"></i> </a>
        </div>
    </div> 
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/tether.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/owl.carousel.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="javascript/waypoints.min.js"></script>

    <script type="text/javascript" src="javascript/kinetic.js"></script>
    <script type="text/javascript" src="javascript/jquery-countTo.js"></script>
    <script type="text/javascript" src="javascript/jquery.owl-filter.js"></script>
    <script type="text/javascript" src="javascript/jquery.fancybox.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script> 
    <script type="text/javascript" src="javascript/jquery-validate.js"></script>
    <script type="text/javascript" src="javascript/main.js"></script>
    <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="revolution/js/slider_v3.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript">
        var domain_address="http://localhost/~rahees/carencure/";
    </script>
    <script type="text/javascript">
        $("#location_id").select2({
            width: '100%',
            ajax: {
                url: domain_address+'admin/location_list.php',
                dataType: 'json',
                method: 'post',
                delay: 250,
                data: function(data) {
                    return {
                        search_tag: data.term,
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: false
            },
        });
        $("#showroom_id").select2({
            width: '100%',
            ajax: {
                url: domain_address+'admin/showroom_list.php',
                dataType: 'json',
                method: 'post',
                delay: 250,
                data: function(data) {
                    return {
                        search_tag : data.term,
                        location_id:$('#location_id').val(),
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: false
            },
        });
        $('#find_button').click(function(){
            var url_address = domain_address+"admin/get_showroom.php?id="+$('#showroom_id').val();
            $.get(url_address, function(response) {
                $('#location_name').text(response.showroom.location);
                $('#showroom_name').text(response.showroom.name);
                $('#showroom_landmark').text(response.showroom.landmark);
                $('#showroom_contact').text(response.showroom.contact);
                $('#showroom_address').attr('src',response.showroom.address);
                $('#working_hours').attr('src',domain_address+'images/'+response.showroom.working_hours+'hr.svg');
                $('#image_area').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
                $('#image_area').find('.owl-stage-outer').children().unwrap();
                var image_area_div='';
                $.each(response.images,function(key,value){
                    image_area_div+='<div class="images-item">';
                    image_area_div+='<img src="'+domain_address+'/admin/uploads/'+value+'" alt="">';
                    image_area_div+='</div>';
                });
                $('#image_area').html(image_area_div);
                $(".owl-carousel-4").owlCarousel({
                    autoplay:true,
                    dots:false,
                    nav: true,
                    margin: 0,
                    loop:true,
                    items:1,
                });
            }, "json");
        });
        $('#find_button').click();
    </script>
</body>
</html>