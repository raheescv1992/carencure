<?php require('admin/connection.php'); ?>
<?php 
session_start();
if(!isset($_SESSION["username"])){
    header("Location: ".$domain_name."login.php");
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="">
    <title>Care n Cure Pharmacy</title>
    <link rel="shortcut icon" type="image/png" href="favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="stylesheet/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/fancybox.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/style.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/responsive.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/animate.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/et-line.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/jquery-ui.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script type="text/javascript" src="js/jquery-3.3.1.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
</head>
<body >
    <div class="boxed">
        <div class="top style2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-page">
                            <a href="#" target="_self" class="tp-caption flat-button-slider bg-blue"data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'data-x="['left','left','left','left']" data-hoffset="['35','35','35','15']"data-y="['middle','middle','middle','middle']" data-voffset="['130','140','140','110']"data-fontsize="['14','14','14','14']"data-width="['auto']"data-height="['auto']">
                                Division of Care n Cure Group
                            </a>
                        </div>
                        <ul class="flat-social">
                            <li> <a href="#" title=""><i class="fa fa-facebook-f"></i></a> </li>
                            <li> <a href="#" title=""><i class="fa fa-google-plus"></i></a> </li>
                            <li> <a href="#" title=""><i class="fa fa-linkedin"></i></a> </li>
                            <li> <a href="#" title=""><i class="fa fa-rss"></i></a> </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div id="logo" class="logo">
                            <a href="#" title="">
                                <img src="images/logo.png" alt="" />
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="iconbox style2 v2">
                            <div class="iconbox-icon">
                                <i class="fa fa-paper-plane-o"></i>
                            </div>
                            <div class="iconbox-content">
                                <h4>+ 974 44507442/3</h4>
                                <p>info@carencureqatar.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="iconbox style2 v2">
                            <div class="iconbox-icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <div class="iconbox-content">
                                <h4>+ 974 44507442/3  </h4>
                                <p> Customer Care</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <header id="header" class="header style1 v1 bg-color">
            <div class="container">
                <div class="row">
                    <div class="header-wrap">
                        <div class="col-md-12">
                            <div class="flat-header-wrap">
                                <div class="nav-wrap">
                                    <div class="btn-menu">
                                        <span></span>
                                    </div>
                                    <nav id="mainnav" class="mainnav">
                                        <ul class="menu">
                                            <li> <a href="http://www.pharmacy.technoastra.com/" title="">Home</a> </li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/find-us/" title="">FIND US</a> </li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/med-club/" title="">MED CLUB</a> </li>
                                            <li> <a href="http://carencurewebstore.com/" target="_blank" title="">WEBSTORE</a> </li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/news-events/"  title="">News & Events</a> </li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/promotion/"  title=""> promotion</a></li>
                                            <li> <a href="http://www.pharmacy.technoastra.com/contact-us/" title="">CONTACT US</a> </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="page-title parallax parallax2">
            <div class="title-heading">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title-heading">
                                <h1 class="h1-title">Admin</h1>
                            </div>
                            <ul class="breadcrumbs">
                                <li><a href="#" title="">Home<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                <li>Admin Panel</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="flat-row flat-contact" style="padding-top:15px; padding-bottom:30px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="border-bottom: solid 1px #136bcc; background:#f9f6f6; padding-top:20px;">
                        <div class="flat-title style1 center">
                            <h2>Master Page </h2>
                        </div>
                        <div class="flat-contact-form" style="padding:0px; margin-top:0px;">
                            <ul class="nav nav-tabs">
                                <li class=""><a data-toggle="tab" href="#Location_tab">Location</a></li>
                                <li class=""><a data-toggle="tab" href="#Showroom_tab">Showroom</a></li>
                                <li class="active"><a data-toggle="tab" href="#Showroom_images_tab">Gallery</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="Location_tab" class="tab-pane fade in ">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel-group" id="location_panel">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#location_panel" href="#collapseLocation" class="collapsed"><h3>Location +</h3></a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseLocation" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-lg-10">
                                                                    <label>Location Name</label>
                                                                    <input type="text" autofocus id='location_name' name="">
                                                                </div>
                                                                <div class="col-lg-2"><br>
                                                                    <button class="btn btn-success" id='add_Location'>Add Location</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <input type="hidden" id='location_table_edit_id' name="">
                                                <table class="table table-bordered table-hover table-striped" id='LocationdataTable' width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Name</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="Showroom_tab" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel-group" id="Showroom_panel">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#Showroom_panel" href="#collapseShowRoom" class="collapsed"><h3>ShowRoom +</h3></a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseShowRoom" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label>Location</label>
                                                                    <input type="text" class="show_room_class" autofocus id='show_room_location_name' placeholder="Enter Location Name" >
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label>Showroom</label>
                                                                    <input type="text" class="show_room_class" placeholder="Enter Showroom Name" id='show_room_name'>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label>Landmark</label>
                                                                    <input type="text" class="show_room_class" placeholder="Enter Landmark" id='show_room_landmark'>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label>Contact</label>
                                                                    <input type="text" class="show_room_class" autofocus id='show_room_contact' placeholder="Enter Location Name" >
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <label>Working Time</label>
                                                                    <select class="data-trigger" id="show_room_working_time">
                                                                        <option value="12">12 Hours</option>
                                                                        <option value="24">24 Hours</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <label>Address</label>
                                                                    <input type="text" class="show_room_class" placeholder="Enter Address" id='show_room_address'>
                                                                </div>
                                                                <div class="col-lg-1"><br>
                                                                    <button class="btn btn-success showroom_button" id='add_Showroom'>Add</button>
                                                                    <button class="btn btn-success showroom_button" style="display: none" value="0" id='edit_Showroom'>Edit</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover table-striped" id='ShowroomdataTable' width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Location</th>
                                                            <th>ShowRoom</th>
                                                            <th>Contact</th>
                                                            <th>LandMark</th>
                                                            <th>Working Time</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="Showroom_images_tab" class="tab-pane fade in active">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel-group" id="Showroom_image_panel">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h5 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#Showroom_image_panel" href="#collapseShowRoomImage" class="collapsed"><h3>ShowRoom +</h3></a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseShowRoomImage" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <form method="post" id="ShowRoomImageForm" enctype="multipart/form-data" action="admin/showroom_image_upload.php">
                                                                <div class="row">
                                                                    <div class="col-lg-5">
                                                                        <label>Showroom</label>
                                                                        <select data-trigger="" required name="showroom_id" id="showroom_id">
                                                                            <option value="">Showroom</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <label>Images</label>
                                                                        <input type="file" name="images[]" id="show_room_images" multiple >
                                                                    </div>
                                                                    <div id="status"></div>
                                                                    <div class="gallery" id="imagesPrev"></div>
                                                                    <div class="col-lg-1"><br>
                                                                        <button type="submit" name="submit" class="btn btn-success" id='add_ShowroomImage'>Add</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <div id="images_preview"></div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover table-striped" id='ShowroomImagedataTable' width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>ShowRoom</th>
                                                            <th>Image</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-ft widget-about">
                            <div id="logo-ft">
                                <a href="#" title="">
                                    <img src="images/logo-ft.png" alt="">
                                </a>
                            </div>
                            <p>We have a dedicated corporate division that caters to the medical needs of over 96 major corporate clients in Qatar. We provide our corporate clients with customized health . 
                            </p>
                            <ul class="social">
                                <li> <a href="#" title=""> <i class="fa fa-facebook-f"></i> </a> </li>
                                <li> <a href="#" title=""> <i class="fa fa-twitter"></i> </a> </li>
                                <li> <a href="#" title=""> <i class="fa fa-linkedin"></i> </a> </li>
                                <li> <a href="#" title=""> <i class="fa fa-google-plus"></i> </a> </li>
                            </ul>   
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-ft widget-services">
                            <h3 class="title">Quicklink</h3>
                            <ul class="one-half first">
                                <li><a href="#" title="">Home</a></li>
                                <li><a href="#" title="">Promotion</a></li>
                                <li><a href="" title=""> Med Club</a> </li>

                            </ul>
                            <ul class="one-half">
                                <li><a href="#" title="">Find us</a></li>
                                <li><a href="#" title="">Contact us</a></li>
                                <li><a href="#" title="">News & Evants</a></li>
                                <li> <a href="http://carencurewebstore.com/" target="_blank" title="">Webstore</a> </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-ft widget-lastest">
                            <h3 class="title">Latest Twitter</h3>
                            <a class="twitter-timeline" data-height="200" data-theme="dark" href="https://twitter.com/CareNCureQatar">Tweets by bloom_entertain</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-ft widget-subscribe">
                            <h3 class="title"> Get in touch with us</h3>
                            CARE N CURE QATAR <br>
                            P. O. Box 23094, Doha-Qatar<br>
                            T: + 974 44507442/3 <br>
                            F: +974 44507445<br>
                            E: info@carencureqatar.com <br>
                            W: www.carencureqatar.com<br>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright">
                            <p>© Copyright <a href="#" title="">Care n Cure 2018</a>. All Rights Reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="button-go-top">
            <a href="#" title="" class="go-top"> <i class="fa fa-chevron-up"></i> </a>
        </div>
    </div> 

    <script type="text/javascript" src="javascript/tether.min.js"></script>
    <script type="text/javascript" src="javascript/owl.carousel.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="javascript/waypoints.min.js"></script>

    <script type="text/javascript" src="javascript/kinetic.js"></script>
    <script type="text/javascript" src="javascript/jquery-countTo.js"></script>
    <script type="text/javascript" src="javascript/jquery.owl-filter.js"></script>
    <script type="text/javascript" src="javascript/jquery.fancybox.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script> 
    <script type="text/javascript" src="javascript/jquery-validate.js"></script>
    <script type="text/javascript" src="javascript/main.js"></script>


    <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="revolution/js/slider_v3.js"></script>

    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>

    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">
        var domain_address="http://localhost/carencure/";
        $('.collapse').on('show.bs.collapse', function () {
            $(this).find('[autofocus]').focus().select();
        });
    </script>
    <script type="text/javascript">
        var LocationdataTable=$('#LocationdataTable').dataTable({
            "processing" : true,
            "serverSide" : true,
            "fixedHeader": true,
            "lengthMenu" : [[ 25, 50, 100,200], [ 25, 50, 100,200, ]],
            "ajax": {
                "url":domain_address+"admin/location_datatable_ajax.php",
                "dataType":"json",
                "type":"POST",
                error: function(){
                    $(".LocationdataTable-error").html("");
                    $("#LocationdataTable").append('<tbody class="LocationdataTable-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#LocationdataTable_processing").css("display","none");

                },
                data:function(d){
                    d.table_id=$('#location_table_edit_id').val();
                },
            },
            "columns":[
            {"data":"key",'visible':true},
            {"data":"name"},
            {"data":"action"},
            ],
            "columnDefs": [
            { "targets": [ 0 ],"width": "10%", },
            { "targets": [ 1 ],"width": "80%", },
            { "targets": [ 2 ],"width": "10%", },
            ],
        });
        $(document).on('click', '.edit_location', function() {
            var table_id = $(this).attr('table_id');
            $('#location_table_edit_id').val(table_id);
            LocationdataTable.fnDraw();
        });
        $('#add_Location').click(function(){
            if (!$('#location_name').val()) {$('#location_name').focus(); return false; }
            var data = {
                'name':$('#location_name').val()
            };
            var url_address = domain_address+"admin/location_add_ajax.php";
            $.post(url_address, data, function(response) {
                if (response.result != 'success') {alert(response.result); return false; }
                LocationdataTable.fnDraw();
                $('#location_name').val('').focus();
            }, "json");
        });
        $(document).on('click', '.ok_location', function() {
            var data= {
                id  : $(this).attr('table_id'),
                name: $('#edit_location_name').val(),
            };
            var url_address = domain_address+'admin/location_update_ajax.php';
            $.post(url_address, data, function(response) {
                if (response.result != 'success') {alert(response.result); return false; }
                $('#location_table_edit_id').val(0);
                LocationdataTable.fnDraw();
            }, "json");
        });
        $(document).on('keypress', '#edit_location_name', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {$('.ok_location').click(); return false; }
        });
        $(document).on('click', '.delete_location', function() {
            if (!confirm("Are you sure?")) {return false; } 
            var data= {
                id  : $(this).attr('table_id'),
            };
            var url_address = domain_address+'admin/location_delete_ajax.php';
            $.post(url_address, data, function(response) {
                if (response.result != 'success') {alert(response.result); return false; }
                LocationdataTable.fnDraw();
            }, "json");
        });
        $(document).on('keypress', '#location_name', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {$('#add_Location').click(); return false; }
        });
    </script>
    <script type="text/javascript">
        var ShowroomdataTable=$('#ShowroomdataTable').dataTable({
            "processing": true,
            "serverSide": true,
            "fixedHeader": true,
            "lengthMenu": [[ 25, 50, 100,200], [ 25, 50, 100,200, ]],
            "ajax": {
                "url":domain_address+"admin/showroom_table_ajax.php",
                "dataType":"json",
                "type":"POST",
                error: function(){
                    $(".ShowroomdataTable-error").html("");
                    $("#ShowroomdataTable").append('<tbody class="ShowroomdataTable-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#LocationdataTable_processing").css("display","none");

                },
                data:function(d){
                },
            },
            "columns":[
            {"data":"key",'visible':true},
            {"data":"location_id"},
            {"data":"name"},
            {"data":"contact"},
            {"data":"landmark"},
            {"data":"working_hours"},
            {"data":"action"},
            ],
            "columnDefs": [
            { "targets": [ 1 ],"width": "15%", },
            { "targets": [ 2 ],"width": "25%", },
            ],
        });
        $( "#show_room_location_name" ).autocomplete({
            source:domain_address+'admin/get_location_lists.php',
            select: function( event, ui ) {
                event.preventDefault();
                $("#show_room_location_name").val(ui.item.value);
                $("#show_room_name").focus();
            },
        });
        $(document).on('keypress', '.show_room_class', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                if($('#add_Showroom:visible').length) {
                    $('#add_Showroom').click();                     
                } else {
                    $('#edit_Showroom').click(); 
                }
                return false; }
            });
        $('#show_room_working_time').change(function(){
            $('#show_room_address').focus();
        });
        $('#add_Showroom').click(function(){
            if (!$('#show_room_location_name').val()) {$('#show_room_location_name').focus(); return false; }
            if (!$('#show_room_name').val()) {$('#show_room_name').focus(); return false; }
            if (!$('#show_room_landmark').val()) {$('#show_room_landmark').focus(); return false; }
            if (!$('#show_room_contact').val()) {$('#show_room_contact').focus(); return false; }
            if (!$('#show_room_address').val()) {$('#show_room_address').focus(); return false; }
            var data = {
                'location_name':$('#show_room_location_name').val(),
                'name'         :$('#show_room_name').val(),
                'landmark'     :$('#show_room_landmark').val(),
                'contact'      :$('#show_room_contact').val(),
                'working_hours':$('#show_room_working_time').val(),
                'address'      :$('#show_room_address').val(),
            };
            var url_address = domain_address+"admin/showroom_add_ajax.php";
            $.post(url_address, data, function(response) {
                if (response.result != 'success') {alert(response.result); return false; }
                ShowroomdataTable.fnDraw();
                LocationdataTable.fnDraw();
                $('.show_room_class').val('');
                $('#show_room_location_name').focus();
            }, "json");
        });
        $(document).on('click', '.edit_showroom', function() {
            $('#edit_Showroom').val($(this).attr('table_id'));
            var url_address = domain_address+'admin/get_showroom.php?id='+$(this).attr('table_id');
            $.get(url_address, function(response) {
                $('#collapseShowRoom').attr('class','panel-collapse collapse in');
                $('#show_room_location_name').val(response.showroom.location);
                $('#show_room_name').val(response.showroom.name);
                $('#show_room_landmark').val(response.showroom.landmark);
                $('#show_room_contact').val(response.showroom.contact);
                $('#show_room_working_time').val(response.showroom.working_hours).change();
                $('#show_room_address').val(response.showroom.address);
                $('#add_Showroom').hide();
                $('#edit_Showroom').show();
            }, "json");
        });
        $('#edit_Showroom').click(function(){
            if (!$('#edit_Showroom').val()) { alert('please Select A Row'); return false; }
            if (!$('#show_room_location_name').val()) {$('#show_room_location_name').focus(); return false; }
            if (!$('#show_room_name').val()) {$('#show_room_name').focus(); return false; }
            if (!$('#show_room_landmark').val()) {$('#show_room_landmark').focus(); return false; }
            if (!$('#show_room_contact').val()) {$('#show_room_contact').focus(); return false; }
            if (!$('#show_room_address').val()) {$('#show_room_address').focus(); return false; }
            var data = {
                'id'           :$('#edit_Showroom').val(),
                'location_name':$('#show_room_location_name').val(),
                'name'         :$('#show_room_name').val(),
                'landmark'     :$('#show_room_landmark').val(),
                'contact'      :$('#show_room_contact').val(),
                'working_hours':$('#show_room_working_time').val(),
                'address'      :$('#show_room_address').val(),
            };
            var url_address = domain_address+"admin/showroom_update_ajax.php";
            $.post(url_address, data, function(response) {
                if (response.result != 'success') {alert(response.result); return false; }
                ShowroomdataTable.fnDraw();
                LocationdataTable.fnDraw();
                $('.show_room_class').val('');
                $('#add_Showroom').show();
                $('#edit_Showroom').val(0);
                $('#edit_Showroom').hide();
                $('#show_room_location_name').focus();
            }, "json");
        });
        $(document).on('click', '.delete_showroom', function() {
            if (!confirm("Are you sure?")) {return false; } 
            var data= {
                id  : $(this).attr('table_id'),
            };
            var url_address = domain_address+'admin/showroom_delete_ajax.php';
            $.post(url_address, data, function(response) {
                if (response.result != 'success') {alert(response.result); return false; }
                ShowroomdataTable.fnDraw();
            }, "json");
        });
    </script>
    <script type="text/javascript">
        var ShowroomImagedataTable=$('#ShowroomImagedataTable').dataTable({
            "processing" : true,
            "serverSide" : true,
            "fixedHeader": true,
            "lengthMenu" : [[ 25, 50, 100,200], [ 25, 50, 100,200, ]],
            "ajax": {
                "url":domain_address+"admin/showroom_image_table_ajax.php",
                "dataType":"json",
                "type":"POST",
                error: function(){
                    $(".ShowroomImagedataTable-error").html("");
                    $("#ShowroomImagedataTable").append('<tbody class="ShowroomImagedataTable-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#ShowroomImagedataTable_processing").css("display","none");

                },
                data:function(d){
                    d.showroom_id=$('#showroom_id').val();
                },
            },
            "columns":[
            {"data":"key",'visible':true},
            {"data":"showroom"},
            {"data":"image"},
            {"data":"action"},
            ],
            "columnDefs": [
            { "targets": [ 1 ],"width": "50%", },
            { "targets": [ 2 ],"width": "40%", },
            ],
        });
        $("#showroom_id").select2({
            width: '100%',
            ajax: {
                url: domain_address+'admin/showroom_list.php',
                dataType: 'json',
                method: 'post',
                delay: 250,
                data: function(data) {
                    return {
                        search_tag : data.term,
                        location_id:$('#location_id').val(),
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: false
            },
        });
        $('#showroom_id').change(function(){
            ShowroomImagedataTable.fnDraw();
        });
        $(document).on('click', '.delete_showroom_image', function() {
            if (!confirm("Are you sure?")) {return false; } 
            var data= {
                id  : $(this).attr('table_id'),
            };
            var url_address = domain_address+'admin/showroom_image_delete_ajax.php';
            $.post(url_address, data, function(response) {
                if (response.result != 'success') {alert(response.result); return false; }
                ShowroomImagedataTable.fnDraw();
            }, "json");
        });
    </script>
</body>
</html>