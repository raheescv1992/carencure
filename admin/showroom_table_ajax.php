<?php require('connection.php'); ?>
<?php
$requestData= $_REQUEST;
$columns =[];
$columns[]='showrooms.id'; 
$columns[]='locations.name'; 
$columns[]='showrooms.name'; 
$columns[]='showrooms.contact'; 
$columns[]='showrooms.landmark'; 
$columns[]='showrooms.working_hours'; 
$columns[]='showrooms.id';
$sql = "SELECT * ";
$sql.=" FROM showrooms";
$query=mysqli_query($connection, $sql) or die("showroom_table_ajax.php: get showrooms");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;
if( !empty($requestData['search']['value']) ) {
    $sql = "SELECT locations.name,showrooms.* ";
    $sql.=" FROM showrooms";
    $sql.=" INNER JOIN locations ON showrooms.location_id = locations.id WHERE showrooms.name LIKE '%".$requestData['search']['value']."%' ";
    $query=mysqli_query($connection, $sql) or die("showroom_table_ajax.php: get showrooms");
    $totalFiltered = mysqli_num_rows($query); 
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    $query=mysqli_query($connection, $sql) or die("showroom_table_ajax.php: get showroomss");
} else {    
    $sql = "SELECT locations.name,showrooms.* ";
    $sql.=" FROM showrooms INNER JOIN locations ON showrooms.location_id = locations.id";
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    $query=mysqli_query($connection, $sql) or die("showroom_table_ajax.php: get showrooms");
}
$data = array();
while( $row=mysqli_fetch_array($query) ) { 
    $nestedData=array(); 
    $nestedData['key']           =$row["id"];
    $nestedData['name']          =$row["name"];
    $nestedData['contact']       =$row["contact"];
    $nestedData['landmark']      =$row["landmark"];
    $nestedData['location_id']   =$row[0];
    $nestedData['working_hours'] =$row["working_hours"];
    $nestedData['action']        ='<span><i table_id="'.$row['id'].'" class="fa fa-2x fa-edit edit_showroom"></i></span>';
    $nestedData['action']       .='<span class="pull-right"><i table_id="'.$row['id'].'" class="fa fa-2x fa-trash-o delete_showroom"></i></span>';
    $data[] = $nestedData;
}
$json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData), 
    "recordsFiltered"=>intval($totalFiltered),
    "data"           =>$data 
);
echo json_encode($json_data);
?>