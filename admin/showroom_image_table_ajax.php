<?php require('connection.php'); ?>
<?php
$requestData= $_REQUEST;
$showroom_id= $_REQUEST['showroom_id'];
$columns =[];
$columns[]='id'; 
$columns[]='image'; 
$columns[]='image'; 
$columns[]='id';
$sql = "SELECT * ";
$sql.=" FROM showroom_images";
$query=mysqli_query($connection, $sql) or die("location_datatable_ajax.php: get showroom_images");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;
if( !empty($requestData['search']['value']) ) {
    $sql = "SELECT * ";
    $sql.=" FROM showroom_images";
    $sql.=" INNER JOIN showrooms ON showrooms.id = showroom_images.showroom_id";
    $sql.=" WHERE image LIKE '%".$requestData['search']['value']."%' ";
    $query=mysqli_query($connection, $sql) or die("location_datatable_ajax.php: get showroom_images");
    $totalFiltered = mysqli_num_rows($query); 
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    $query=mysqli_query($connection, $sql) or die("location_datatable_ajax.php: get showroom_images");
} else {    
    $sql = "SELECT showroom_images.*,showrooms.name as showroom ";
    $sql.=" FROM showroom_images";
    $sql.=" INNER JOIN showrooms ON showrooms.id = showroom_images.showroom_id";
    if($showroom_id)
    {
        $sql.=" WHERE showroom_id='$showroom_id'";
    }
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    $query=mysqli_query($connection, $sql) or die("location_datatable_ajax.php: get showroom_images");
}
$data = array();
$i=0;
while( $row=mysqli_fetch_array($query) ) {
    $i++;
    $nestedData=array(); 
    $nestedData['id']      =$row["id"];
    $nestedData['key']     =$row["id"];
    $nestedData['image']   ="<img style='width:100%;height:100%' src='".$domain_name."/admin/uploads/".$row["image"]."'>";
    $nestedData['showroom']=$row["showroom"];
    $nestedData['action']  ='<span class="pull-right"><i table_id="'.$row['id'].'" class="fa fa-2x fa-trash-o delete_showroom_image"></i></span>';
    $data[] = $nestedData;
}
$json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData), 
    "recordsFiltered"=>intval($totalFiltered),
    "data"           =>$data 
);
echo json_encode($json_data);
?>