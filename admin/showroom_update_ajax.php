<?php require('connection.php'); ?>
<?php
try {
	if(!$_POST['location_name']) throw new Exception("location_name required", 1);
	if(!$_POST['name']) throw new Exception("Name required", 1);
	if(!$_POST['landmark']) throw new Exception("landmark required", 1);
	if(!$_POST['contact']) throw new Exception("contact required", 1);
	if(!$_POST['address']) throw new Exception("address required", 1);
	if(!$_POST['id']) throw new Exception("id required", 1);

	$id            = $_POST['id'];
	$location_name = $_POST['location_name'];
	$name          = $_POST['name'];
	$landmark      = $_POST['landmark'];
	$contact       = $_POST['contact'];
	$working_hours = $_POST['working_hours'];
	$address       = $_POST['address'];

	$location_sql="SELECT id FROM locations where name='$location_name'";
	location_get_area :
	$location_get_query=mysqli_query($connection, $location_sql) or die("showroom_add_ajax.php: get locations");
	$location_row = mysqli_fetch_array($location_get_query);
	$location_rows = mysqli_num_rows($location_get_query);
	if($location_rows!=0)
	{
		$location_id=$location_row['id'];
	}
	else
	{
		$q = "INSERT INTO locations (name) VALUES ('$location_name')";
		$query = mysqli_query($connection, $q);
		goto location_get_area;
	}
	$sql="SELECT * FROM showrooms where name='$name' and id!='$id'";
	$query=mysqli_query($connection, $sql) or die("location_add_ajax.php: get locations");
	$num_rows = mysqli_num_rows($query);
	if($num_rows >= 1) throw new Exception("Showroom Already Added", 1);

	$q = "UPDATE showrooms SET";
	$q.= " name='$name',";
	$q.= "location_id='$location_id',";
	$q.= "landmark='$landmark',";
	$q.= "contact='$contact',";
	$q.= "working_hours='$working_hours',";
	$q.= "address='$address'";
	$q.= " where id='".$id."'";
	$query = mysqli_query($connection, $q);
	if(!$query) throw new Exception("Error Processing Request", 1);
	$return['result']="success";
} catch (Exception $e) {
	$return['result']=$e->getMessage();
}
echo json_encode($return);
?>