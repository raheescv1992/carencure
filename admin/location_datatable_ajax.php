<?php require('connection.php'); ?>
<?php
$requestData= $_REQUEST;
$columns =[];
$columns[]='id'; 
$columns[]='name'; 
$columns[]='id';
$sql = "SELECT * ";
$sql.=" FROM locations";
$query=mysqli_query($connection, $sql) or die("location_datatable_ajax.php: get locations");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;
if( !empty($requestData['search']['value']) ) {
    $sql = "SELECT * ";
    $sql.=" FROM locations";
    $sql.=" WHERE name LIKE '%".$requestData['search']['value']."%' ";
    $query=mysqli_query($connection, $sql) or die("location_datatable_ajax.php: get locations");
    $totalFiltered = mysqli_num_rows($query); 
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    $query=mysqli_query($connection, $sql) or die("location_datatable_ajax.php: get locations");
} else {    
    $sql = "SELECT * ";
    $sql.=" FROM locations";
    $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
    $query=mysqli_query($connection, $sql) or die("location_datatable_ajax.php: get locations");
}
$data = array();
$i=0;
while( $row=mysqli_fetch_array($query) ) { 
    $i++;
    $nestedData=array(); 
    $nestedData['id']     =$row["id"];
    $nestedData['key']    =$row["id"];
    $nestedData['name']   =$row["name"];
    $nestedData['action'] ='<span><i table_id="'.$row['id'].'" class="fa fa-2x fa-edit edit_location"></i></span>';
    $nestedData['action'].='<span class="pull-right"><i table_id="'.$row['id'].'" class="fa fa-2x fa-trash-o delete_location"></i></span>';
    if($requestData['table_id']==$row['id'])
    {
        $nestedData['name']  ="<input type='text' value='".$row["name"]."' id='edit_location_name' autofocus>";
        $nestedData['action']='<span><i table_id="'.$row['id'].'" class="fa fa-2x fa-check ok_location" ></i></span>';
    }
    $data[] = $nestedData;
}
$json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData), 
    "recordsFiltered"=>intval($totalFiltered),
    "data"           =>$data 
);
echo json_encode($json_data);
?>